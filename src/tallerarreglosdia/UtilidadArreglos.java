/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerarreglosdia;

/**
 *
 * @author carloaiza
 */
public class UtilidadArreglos {

    public static boolean esPrimo(int numero) {
        int contDivisores = 0;
        for (int i = numero / 2; i >= 1; i--) {
            if (numero % i == 0) {
                contDivisores++;
            }
        }
        return contDivisores ==1;
    }
    
    public static int[] encontrarPrimosRango(int cuantos, int rangoIni, int rangoFinal)
    {
        int[] primos= new int[cuantos];
        int cont=0;
        for(int i = rangoIni; i <= rangoFinal; i++)
        {
            if(esPrimo(i))
            {
                primos[cont]=i;
                cont++;
            }
            if(cont == cuantos)
            {
                break;
            }
        }    
        return primos;
    }
    
    public static String mostrarDatosArreglo(int[] arreglo)
    {
        String texto="";
        for(int i=0; i< arreglo.length; i++)
        {
            texto+= "\n arr["+i+"]= "+ arreglo[i];
        }
        return texto;
    }
    
    
    public static int sumarDigitos(int numero)
    {
        int suma=0;
        while(numero > 10)
        {
            suma= suma + (numero % 10);            
            numero = numero /10;            
        }
        
        suma= suma+numero;        
        return suma;
        
    }
    
}
